import axios from "axios";

const baseURL = "https://api.trello.com";

export const getData = async (path) => {
  try {
    const { data } = await axios.get(`${baseURL}/${path}`);
    return data;
  } catch (error) {
    if (error.response) {
      console.log(error.response.data);
    } else if (error.request) {
      // The request was made but no response was received
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log("Error", error.message);
    }
  }
};

export const postData = async (path, payload) => {
  try {
    const { data } = await axios.post(`${baseURL}/${path}`, payload);
    return data;
  } catch (error) {
    if (error.response) {
      console.log(error.response.data);
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log("Error", error.message);
    }
  }
};

export const putData = async (path, payload) => {
  try {
    const { data } = await axios.put(`${baseURL}/${path}`, payload);
    return data;
  } catch (error) {
    if (error.response) {
      console.log(error.response.data);
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log("Error", error.message);
    }
  }
};

export const deleteData = async (path) => {
  try {
    const { data } = await axios.delete(`${baseURL}/${path}`);
    return data;
  } catch (error) {
    if (error.response) {
      console.log(error.response.data);
    } else if (error.request) {
      // The request was made but no response was received
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log("Error", error.message);
    }
  }
};
