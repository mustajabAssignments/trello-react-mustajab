import React, { Component } from "react";

import { getData, deleteData, postData } from "./api";

// GLOBAL VARIABLES
const key = "684457d51da2ac591248ba0d51d67c27";
const token =
  "ATTA909fbeb2ee4b69f0c15b50dfaed927eceac71fc707c2681f7406ed02b97f486109F2B513";

class Cards extends Component {
  state = {
    data: [],
  };

  async componentDidMount() {
    const path = `1/lists/${this.props.id}/cards?key=${key}&token=${token}`;
    const data = await getData(path);
    this.setState({
      data,
    });
  }

  async handleDelete(event) {
    const id = event.target.id;
    console.log(id);
    const path = `1/cards/${id}/?key=${key}&token=${token}`;
    await deleteData(path);
    this.setState({
      data: this.state.data.filter((card) => card.id !== id),
    });
  }

  async handleAdd(event) {
    event.preventDefault();
    const name = event.target.card.value;
    event.target.card.value = "";
    if (!name) return;
    const path = `1/cards?idList=${this.props.id}&key=${key}&token=${token}`;
    const param = { name: name };
    const data = await postData(path, param);
    this.setState({
      data: [...this.state.data, data],
    });
  }

  render() {
    return (
      <>
        {this.state.data.map((card) => {
          return (
            <div key={card.id} className="card" style={{ margin: "1rem" }}>
              <span style={{ textAlign: "center" }}>{card.name}</span>
              <button
                className="btn btn-danger"
                onClick={(event) => this.handleDelete(event)}
                id={card.id}
                style={{
                  border: "1px solid black",
                }}
              >
                remove card
              </button>
            </div>
          );
        })}
        <form
          onSubmit={(event) => this.handleAdd(event)}
          style={{ margin: "1rem" }}
        >
          <div className="form-group">
            <input
              className="form-control"
              type="text"
              name="card"
              placeholder="enter the name of card"
            />
            <button
              type="submit"
              className="btn btn-dark"
              style={{ marginLeft: "auto" }}
            >
              add new card
            </button>
          </div>
        </form>
      </>
    );
  }
}

export default Cards;
