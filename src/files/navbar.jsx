import React from "react";

import icon from "./trello-icon.svg";
import { Link } from "react-router-dom";

export default function navbar() {
  return (
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div class="container">
        <Link to="/">
          <img src={icon} width="50" height="50" alt="logo" />
          <span>trello</span>
        </Link>
      </div>
    </nav>
  );
}
