import React, { Component } from "react";

import { getData, postData, putData } from "./api";
import Cards from "./cards";

// GLOBAL VARIABLES
const key = "684457d51da2ac591248ba0d51d67c27";
const token =
  "ATTA909fbeb2ee4b69f0c15b50dfaed927eceac71fc707c2681f7406ed02b97f486109F2B513";

class Board extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id,
      data: [],
      didReceive: false,
    };
  }

  async componentDidMount() {
    console.log(this.props);
    const path = `1/boards/${this.state.id}/lists?key=${key}&token=${token}`;
    const data = await getData(path);
    this.setState({
      data,
      didReceive: true,
    });
  }

  async handleAdd(event) {
    event.preventDefault();
    const name = event.target.card.value;
    event.target.card.value = "";
    if (!name) return;
    const path = `1/boards/${this.state.id}/lists?name=${name}&key=${key}&token=${token}`;
    const data = await postData(path);
    this.setState({
      data: [...this.state.data, data],
    });
  }

  async handleDelete(event) {
    const id = event.target.id;
    const path = `1/lists/${id}/closed?key=${key}&token=${token}`;
    const param = { value: true };
    await putData(path, param);
    this.setState({
      data: this.state.data.filter((list) => list.id !== id),
    });
  }

  render() {
    return (
      <>
        <div className="card-group">
          {this.state.data.map((list) => {
            return (
              <div key={list.id} className="card">
                <div className="card-body board">
                  <span className="card-title">{list.name}</span>
                  <button
                    className="btn btn-danger"
                    onClick={(event) => this.handleDelete(event)}
                    id={list.id}
                    style={{ marginLeft: "50%" }}
                  >
                    delete
                  </button>
                  <Cards id={list.id} />
                </div>
              </div>
            );
          })}
        </div>
        <form onSubmit={(event) => this.handleAdd(event)}>
          <div className="form-group">
            <input
              className="form-control-lg"
              type="text"
              name="card"
              placeholder="enter the name of list"
            />
            <button type="submit" className="btn btn-dark">
              add new list
            </button>
          </div>
        </form>
      </>
    );
  }
}

export default Board;
