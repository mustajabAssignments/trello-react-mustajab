import React, { Component } from "react";

import * as API from "./api";
import Board from "./board";

import { Link } from "react-router-dom";

// GLOBAL VARIABLES
const key = "684457d51da2ac591248ba0d51d67c27";
const token =
  "ATTA909fbeb2ee4b69f0c15b50dfaed927eceac71fc707c2681f7406ed02b97f486109F2B513";

class Boards extends Component {
  state = {
    isLoading: true,
    isError: false,
    data: [],
  };

  async componentDidMount() {
    const path = `1/members/me/boards?key=${key}&token=${token}`;
    const data = await API.getData(path);
    this.setState({
      isLoading: false,
      isError: false,
      data,
    });
    !data &&
      this.setState({
        isError: true,
      });
  }

  async handleDelete(event) {
    const id = event.target.id;
    const path = `1/boards/${id}?key=${key}&token=${token}`;
    await API.deleteData(path);
    this.setState({
      data: this.state.data.filter((board) => board.id !== id),
    });
  }

  async handleAdd(event) {
    event.preventDefault();
    const name = event.target.board.value;
    event.target.board.value = "";
    if (!name) return;
    const path = `1/boards/?name=${name}&key=${key}&token=${token}`;
    const data = await API.postData(path);
    this.setState({
      data: [...this.state.data, data],
    });
  }

  render() {
    return (
      <>
        {this.state.isLoading && "Loading..."}
        {this.state.isError && "Error"}
        <div className="card-group">
          {this.state.data.map((board) => {
            return (
              <div className="card" key={board.id}>
                <div className="card-body board">
                  <Link to={`/${board.id}`}>
                    <h5 className="card-title">{board.name}</h5>
                  </Link>
                  <button
                    type="button"
                    className="btn btn-dark"
                    id={board.id}
                    onClick={(event) => this.handleDelete(event)}
                  >
                    delete
                  </button>
                </div>
              </div>
            );
          })}
        </div>

        <form onSubmit={(event) => this.handleAdd(event)}>
          <div className="form-group">
            <input
              class="form-control-lg"
              type="text"
              name="board"
              placeholder="enter the name of board"
            />
            <button type="submit" className="btn btn-dark">
              Add Board
            </button>
          </div>
        </form>
      </>
    );
  }
}

export default Boards;
