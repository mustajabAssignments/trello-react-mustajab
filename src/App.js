import Boards from "./files/boards";

import { BrowserRouter, Switch, Route, Link, Routes } from "react-router-dom";
import Board from "./files/board";
import Navbar from "./files/navbar";

import "bootstrap/dist/css/bootstrap.css";
import "./App.css";

function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <Switch>
        <Route exact path="/" component={Boards} />
        <Route path="/:id" component={Board} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
